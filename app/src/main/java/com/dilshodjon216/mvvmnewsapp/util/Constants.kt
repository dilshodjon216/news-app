package com.dilshodjon216.mvvmnewsapp.util

class Constants {
    companion object{
        const val API_KEY="7cb2f8f79aee4db79b8578c846bd434f"
        const val BASE_URL = "https://newsapi.org"
        const val SEARCH_NEWS_TIME_DELAY=500L
        const val QUERY_PAGE_SIZE=20
    }
}