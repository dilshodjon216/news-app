package com.dilshodjon216.mvvmnewsapp.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dilshodjon216.mvvmnewsapp.models.Article

@Dao
interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(article: Article): Long


    @Query("SELECT *FROM articles")
    fun getAllArticles(): LiveData<List<Article>>

    @Delete
    suspend fun deleteArticle(article: Article)
}