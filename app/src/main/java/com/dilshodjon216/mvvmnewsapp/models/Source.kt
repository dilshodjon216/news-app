package com.dilshodjon216.mvvmnewsapp.models

data class Source(
    val id: Any,
    val name: String
)