package com.dilshodjon216.mvvmnewsapp.respository

import com.dilshodjon216.mvvmnewsapp.api.RetrofitInstance
import com.dilshodjon216.mvvmnewsapp.db.ArticleDatabase
import com.dilshodjon216.mvvmnewsapp.models.Article

class NewsRepository(val db: ArticleDatabase) {
    suspend fun getBreakingNews(countryCode: String, pageNumber: Int) =
            RetrofitInstance.api.getBreakingNews(countryCode, pageNumber)
    suspend fun searchNews(countryCode: String, pageNumber: Int) =
            RetrofitInstance.api.searchForNews(countryCode, pageNumber)

    suspend fun upsert(article: Article) = db.getArticleDao().upsert(article)

    fun getSavedNews() = db.getArticleDao().getAllArticles()

    suspend fun deleteArticle(article: Article) = db.getArticleDao().deleteArticle(article)
}